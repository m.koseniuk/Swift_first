// Задание 1 - Рефакторинг кода

class Field {
    let header: String
    let length: Int
    let placeholder: String
    let code: Int
    let priority: String
    
    init(header: String, length: Int, placeholder: String, code: Int, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
}

let nameField = Field(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: "high")

let surnameField = Field(header: "Surname", length: 25, placeholder: "Type your surname", code: 2, priority: "high")
 
let ageField = Field(header: "Age", length: 3, placeholder: "", code: 3, priority: "middle")

let cityField = Field(header: "City", length: 15, placeholder: "City", code: 4, priority: "low")


// Для проверки:
//print ("nameFieldHeader = \(nameField.header)\nnameFieldLength = \(nameField.length)\nnameFieldPlaceholder = \(nameField.placeholder)\nnameFieldCode = \(nameField.code)\nnameFieldPriority = \(nameField.priority)\n")
//print ("surnameFieldHeader = \(surnameField.header)\nsurnameFieldLength = \(surnameField.length)\nsurnameFieldPlaceholder = \(surnameField.placeholder)\nsurnameFieldCode = \(surnameField.code)\nsurnameFieldPriority = \(surnameField.priority)\n")
//print("ageFieldHeader = \(ageField.header)\nageFieldLength =\(ageField.length)\nageFieldCode = \(ageField.code)\nageFieldPriority =\(ageField.priority)\n")
//print("cityFieldHeader = \(cityField.header)\ncityFieldLength = \(cityField.length)\ncityFieldPlaceholder = \(cityField.placeholder)\ncityFieldPriority = \(cityField.priority)\n")


// Задание 2 - Функция, проверяющая длину поля

class Field2 {
    let header: String
    let length: Int
    var placeholder: String
    let code: Int
    let priority: String
    
    init(header: String, length: Int, placeholder: String, code: Int, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }

    func placeholderLength(placeholder: String) -> Bool {
        if placeholder.count > 25 {
            return false
        }
        self.placeholder = placeholder
        return true
        }
    }
let nameField2 = Field2(header: "Name", length: 25, placeholder: "Тут меньше 25 символов", code: 1, priority: "hight")
let nameField3 = Field2(header: "Name", length: 25, placeholder: "Тут сильно больше 25 символов", code: 1, priority: "hight")

nameField2.placeholderLength(placeholder: nameField2.placeholder)
nameField2.placeholderLength(placeholder: nameField3.placeholder)

print("nameFieldHeader = \(nameField2.header)\nnameFieldLength = \(nameField2.length)\nnameFieldPlaceholder = \(nameField2.placeholder)\nnameFieldCode = \(nameField2.code)\nnameFieldPriority = \(nameField2.priority)\n")
print("nameFieldHeader = \(nameField3.header)\nnameFieldLength = \(nameField3.length)\nnameFieldPlaceholder = \(nameField3.placeholder)\nnameFieldCode = \(nameField3.code)\nnameFieldPriority = \(nameField3.priority)\n")

// Задание 3 - Опционалы

class Field4 {
    let header: String
    let length: Int
    let placeholder: String?
    let code: Int?
    let priority: String
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
}

let nameField4 = Field4(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: "high")

let surnameField4 = Field4(header: "Surname", length: 25, placeholder: "Type your surname", code: 2, priority: "high")
 
let ageField4 = Field4(header: "Age", length: 3, code: 3, priority: "middle")

let cityField4 = Field4(header: "City", length: 15, placeholder: "City", priority: "low")

let extraField4 = Field4(header: "Extra", length: 15, priority: "low")

// Задание 4 - Перечисления (enum)
enum Priority {
    case hight
    case middle
    case low
}
class Field5 {
    let header: String
    let length: Int
    let placeholder: String?
    let code: Int?
    let priority: Priority
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: Priority) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }

}

let nameField5 = Field5(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: Priority.hight)

let surnameField5 = Field5(header: "Surname", length: 25, placeholder: "Type your surname", code: 2, priority: Priority.hight)
 
let ageField5 = Field5(header: "Age", length: 3, placeholder: "", code: 3, priority: Priority.middle)

let cityField5 = Field5(header: "City", length: 15, placeholder: "City", code: 4, priority: Priority.low)

// Задание 5 - Структура

struct Field6 {
    let header: String
    let length: Int
    var placeholder: String
    let code: Int
    
    init(header: String, length: Int, placeholder: String, code: Int) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
    }

    mutating func placeholderLength(placeholder: String) -> Bool {
        if placeholder.count > 25 {
            return false
        }
        self.placeholder = placeholder
        return true
    }
}

var nameField6 = Field6(header: "Struct", length: 25, placeholder: "Структура", code: 1)

nameField6.placeholderLength(placeholder: nameField6.placeholder)
nameField6.placeholderLength(placeholder: nameField6.placeholder + "Куча символов, чтоб было больше 25")
