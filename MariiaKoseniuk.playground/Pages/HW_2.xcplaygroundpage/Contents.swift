//Задача 1
var milkmanPhrase = "Молоко - это полезно"

print(milkmanPhrase)

//Задача 2
var milkPrice: Double = 3

//Задача 3
milkPrice = 4.20

//Задача 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0

profit = milkPrice * Double(milkBottleCount!)
print(profit)

//Задача 5
var employeesList: [String] = ["Иван","Петр","Геннадий","Марфа","Андрей"]

//Задача 6
var isEveryoneWorkHard = false
var workingHours =  40
if workingHours >= 40{
    isEveryoneWorkHard = true
}
print("Сотрудник работал \(workingHours) часов? - \(isEveryoneWorkHard)")

isEveryoneWorkHard = false
workingHours =  30
if workingHours >= 40{
    isEveryoneWorkHard = true
}
print("Сотрудник работал 40 часов? - \(isEveryoneWorkHard)")
