//Задание 1

func fibonacciNumber(count:Int) -> [Int] {
    var number1 = 1
    var number2 = 1
    var storyPointsNumbers: [Int] = [number1, number2]
    
    for _ in 0 ..< count - 2 {
        let number = number1 + number2
        number1 = number2
        number2 = number
        storyPointsNumbers.append(number)
    }
    
    return storyPointsNumbers
}
var fibonacciNumber5 = fibonacciNumber(count:5)
print(fibonacciNumber5)

var fibonacciNumber7 = fibonacciNumber(count:7)
print(fibonacciNumber7)

//Задание 2

let printArrayFor = {
    for item in fibonacciNumber(count: 10) {
            print (item)
    }
    
}

printArrayFor()

